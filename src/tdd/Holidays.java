package tdd;

import java.text.SimpleDateFormat;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Holidays {
	
//	public static void main(String[] args) {
//		System.out.println(convertDateToCorrectFormat ("22/13/2000"));
//		
//		//
//		System.out.println(isSomeDateBetweenPeriods("01/01/1900", "31/12/2100", "27/03/89"));
//		System.out.println(isDateBetweenFixedLimits("27/03/89"));
//	}

	private String dateIn;
	private String dateOut;
	
	public Holidays(String dateIn, String dateOut){
		this.dateIn = dateIn;
		this.dateOut = dateOut;
	}
	
	public Holidays(){
	}
	

	public static DateTime convertDateToCorrectFormat (String date) {
		DateTime dateTime = null;
		DateTimeFormatter format = DateTimeFormat.forPattern("dd/MM/yyyy");
		try {
			dateTime = format.parseDateTime(date);
	    } catch (Exception e) {
	    	throw new IllegalArgumentException();
	    }
	    
		return dateTime;
	}

	public boolean isDateInBeforeOrSameDateOut() {
		DateTime dateInFormat = convertDateToCorrectFormat(dateIn);
		DateTime dateOutFormat = convertDateToCorrectFormat(dateOut);
		return dateInFormat.isBefore(dateOutFormat.plusDays(1));
	}

	public static boolean isDateBetweenFixedLimits(String dateYY) {
		String dateMin = "01/01/1900";
		String dateMax = "31/12/2100";		
		return isSomeDateBetweenPeriods(dateMin, dateMax, dateYY);
	}
	

	public static boolean isSomeDateBetweenPeriods(String dateIn, String dateOut, String someDate) {
		DateTime dateInFormat = convertDateToCorrectFormat(dateIn);
		DateTime dateOutFormat = convertDateToCorrectFormat(dateOut);
		DateTime someDateFormat = convertDateToCorrectFormat(someDate);
		Interval interval = new Interval(dateInFormat, dateOutFormat);
		
		DateTime startInterval = interval.getStart();
		DateTime endInterval = interval.getEnd();
		
		boolean isDateBewtweenInterval = interval.contains(someDateFormat);
		return isDateBewtweenInterval;
	}
	
	public String anotherDateInsideGivenPeriod (String dateInside) {
		if (isDateInBeforeOrSameDateOut() && isDateBetweenFixedLimits(dateInside) ) {
			return isSomeDateBetweenPeriods(dateIn, dateOut, dateInside)?
					"Congratulations! The date "+dateInside+" is inside the period between " + dateIn + " and "+ dateOut 
					: "Oh! we are sorry, this date is outside the period you selected." ;
		}
		return "You need to insert the dates again. Something went wrong!";
	}

	
	public boolean isSomeIntervalBetweenPeriod(String dateIn, String dateOut, String someDateIn, String someDateOut) {
		DateTime dateInFormat = convertDateToCorrectFormat(dateIn);
		DateTime dateOutFormat = convertDateToCorrectFormat(dateOut);
		DateTime someDateInFormat = convertDateToCorrectFormat(someDateIn);
		DateTime someDateOutFormat = convertDateToCorrectFormat(someDateOut);
		Interval interval = new Interval(dateInFormat, dateOutFormat);
		Interval someInterval = new Interval(someDateInFormat, someDateOutFormat);
		
		DateTime startInterval = interval.getStart();
		DateTime endInterval = interval.getEnd();
		DateTime startSomeInterval = someInterval.getStart();
		DateTime endSomeInterval = someInterval.getEnd();
		
		boolean isIntervalBewtweenInterval = interval.contains(someInterval);
		return isIntervalBewtweenInterval;
	}

	public int numberDaysBetweenGivenPeriod() {
		DateTime dateInFormat = convertDateToCorrectFormat(dateIn);
		DateTime dateOutFormat = convertDateToCorrectFormat(dateOut);
		int days = Days.daysBetween(dateInFormat, dateOutFormat).getDays();
		return days;
	}

	public boolean isPeriodInSameMonthAndYear() {	
		DateTime dateInFormat = convertDateToCorrectFormat(dateIn);
		DateTime dateOutFormat = convertDateToCorrectFormat(dateOut);
		if(isPeriodInSameYear()){
			return dateInFormat.getMonthOfYear() == dateOutFormat.getMonthOfYear();
		}
		return false;
	}

	public boolean isPeriodInSameYear() {
		DateTime dateInFormat = convertDateToCorrectFormat(dateIn);
		DateTime dateOutFormat = convertDateToCorrectFormat(dateOut);
		return dateInFormat.getYear() == dateOutFormat.getYear();
	}
	
	
	
}

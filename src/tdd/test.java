
package tdd;

import static org.junit.Assert.*;

import org.hamcrest.core.IsCollectionContaining;
import org.joda.time.DateTime;
import org.joda.time.IllegalFieldValueException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


public class test {

	private Holidays holidays;
	
	@Test
	public void holidaysHasInput() {
		//given
		String dateIn = "27/03/1989";
		String dateOut = "24/03/1986";
		//when
		Holidays holidays = new Holidays(dateIn, dateOut);
		//then
		Assert.assertNotNull(holidays);
	}
	
	@Rule
	public final ExpectedException thrown = ExpectedException.none();

	@Test
	public void dateFormatPointIsNotCorrect() {
		//given
		String datePoint = "27.03.1989";
		//we can test the exception
		thrown.expect(IllegalArgumentException.class);		
		holidays.convertDateToCorrectFormat(datePoint);
	}
	
	@Test
	public void dateFormatHyphenIsNotCorrect() {
		//given
		String dateHyphen = "23-02-1888";
		//we can test the exception
		thrown.expect(IllegalArgumentException.class);		
		holidays.convertDateToCorrectFormat(dateHyphen);
	}

    @Test
    public void dateInvalidLeapYear(){
    	String dateBisiesto = "29/02/2015";   	
    	thrown.expect(IllegalArgumentException.class);
		holidays.convertDateToCorrectFormat(dateBisiesto);
    }
    
    @Test
    public void dateInvalidMoreMonths(){
    	String dateMoreMonths = "22/13/2000";   	
    	thrown.expect(IllegalArgumentException.class);
		holidays.convertDateToCorrectFormat(dateMoreMonths);
    }
    
    
    @Test
    public void dateInvalidMoreDays(){
    	String dateMoreDays = "35/03/2010";
    	thrown.expect(IllegalArgumentException.class);
    	holidays.convertDateToCorrectFormat(dateMoreDays);
    }

	@Test
	public void dateFormatYYIsNotCorrect() {
		String dateYY = "24/03/86";
		//we can test the exception
		boolean dateInvalidYY = holidays.isDateBetweenFixedLimits(dateYY);
		Assert.assertFalse(dateInvalidYY);
	}
    
    @Test 
    public void dateInAfterDateOut() {
    	//given, when, then
    	String dateIn = "04/06/2016";
    	String dateOut = "03/06/2016";    	
    	Holidays holidays = new Holidays(dateIn, dateOut);
    	boolean intervalNegative= holidays.isDateInBeforeOrSameDateOut();
    	Assert.assertFalse(intervalNegative);
    }
    
    @Test 
    public void dateInBeforeDateOut() {
    	//given, when, then
    	String dateIn = "03/06/2016";
    	String dateOut = "04/06/2016";    	
    	Holidays holidays = new Holidays(dateIn, dateOut);
    	boolean intervalPositive= holidays.isDateInBeforeOrSameDateOut();
    	Assert.assertTrue(intervalPositive);
    }
    
    
    @Test 
    public void dateInSameDateOut() {
    	//given, when, then
    	String dateIn = "04/06/2016";
    	String dateOut = "04/06/2016";    	
    	Holidays holidays = new Holidays(dateIn, dateOut);
    	boolean intervalEqual= holidays.isDateInBeforeOrSameDateOut();
    	Assert.assertTrue(intervalEqual);
    }
    
    @Test
    public void someDateOutsidePeriodGiven() {
    	String dateIn = "04/06/2016";
    	String dateOut = "03/06/2017";
    	Holidays holidays = new Holidays(dateIn, dateOut);
    	String dateOutside = "04/06/2017";
    	String resultDateOutPeriod = holidays.anotherDateInsideGivenPeriod(dateOutside);
    	Assert.assertTrue(resultDateOutPeriod.contains("outside"));    	
    }
    
    @Test
    public void someDateInsidePeriodGiven() {
    	String dateIn = "04/06/2016";
    	String dateOut = "03/06/2017";
    	Holidays holidays = new Holidays(dateIn, dateOut);
    	String dateInside = "05/06/2016";
    	String resultDateInPeriod = holidays.anotherDateInsideGivenPeriod(dateInside);
    	Assert.assertTrue(resultDateInPeriod.contains("inside"));    
    }
    
    @Test
    public void someDateLimitPeriodGiven() {
    	String dateIn = "04/06/2016";
    	String dateOut = "03/06/2017";
    	Holidays holidays = new Holidays(dateIn, dateOut);
    	String dateInside = "04/06/2016";
    	String resultDateLimitPeriod = holidays.anotherDateInsideGivenPeriod(dateInside);
    	Assert.assertTrue(resultDateLimitPeriod.contains("inside"));    
    }
    
    @Test
    public void sevenDaysBetweenAWeek() {
    	String dateIn = "01/06/2016";
    	String dateOut = "08/06/2016";
    	Holidays holidays = new Holidays(dateIn, dateOut);
    	int daysInPeriod = holidays.numberDaysBetweenGivenPeriod();
    	Assert.assertEquals(7, daysInPeriod);
    }
    
    @Test
    public void allPeriodInSameMonthAndYear() {
    	String dateIn = "01/02/2016";
    	String dateOut = "20/02/2016";
    	Holidays holidays = new Holidays(dateIn, dateOut);
    	boolean periodFebruary = holidays.isPeriodInSameMonthAndYear();
    	Assert.assertTrue(periodFebruary);
    }
    
    @Test
    public void allPeriodNotInSameMonth() {
    	String dateIn = "01/02/2016";
    	String dateOut = "20/03/2016";
    	Holidays holidays = new Holidays(dateIn, dateOut);
    	boolean marchToFebruary = holidays.isPeriodInSameMonthAndYear();
    	Assert.assertFalse(marchToFebruary);
    }
    
    @Test
    public void allPeriodInSameMonthButNotYear() {
    	String dateIn = "01/02/2016";
    	String dateOut = "20/02/2017";
    	Holidays holidays = new Holidays(dateIn, dateOut);
    	boolean februaryDifferentYear = holidays.isPeriodInSameMonthAndYear();
    	Assert.assertFalse(februaryDifferentYear);
    }
    
    
    @Test
    public void allPeriodInSameYear() {
    	String dateIn = "01/01/2016";
    	String dateOut = "31/12/2016";
    	Holidays holidays = new Holidays(dateIn, dateOut);
    	boolean allPeriodYearSixteen = holidays.isPeriodInSameYear();
    	Assert.assertTrue(allPeriodYearSixteen);
    }
    
    @Test
    public void allPeriodNotInSameYear() {
    	String dateIn = "31/12/2016";
    	String dateOut = "01/01/2017";
    	Holidays holidays = new Holidays(dateIn, dateOut);
    	boolean oneDayBetweenSixteenAndSeveteenYear = holidays.isPeriodInSameYear();
    	Assert.assertFalse(oneDayBetweenSixteenAndSeveteenYear);
    }

    
    
    
	
}
